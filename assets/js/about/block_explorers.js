/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#tx-search-animation', '/assets/js/about/block_explorers/animation/tx_search.json');
    loadAnimation('#addr-search-animation', '/assets/js/about/block_explorers/animation/addr_search.json');
});